
// Karma configurati// Generated on Fri May 15 2015 17:02:23 GMT+0530 (India Standard Time)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
      // bower:js
	"//connect.facebook.net/en_US/sdk.js",
      "app/bower_components/jquery/dist/jquery.js",
      "app/bower_components/angular/angular.js",
      "app/bower_components/angular-messages/angular-messages.js",
      "app/bower_components/angular-resource/angular-resource.js",
      "app/bower_components/angular-sanitize/angular-sanitize.js",
      "app/bower_components/angular-ui-router/release/angular-ui-router.js",
      "app/bower_components/ng-dialog/js/ngDialog.js",
      "app/bower_components/angular-google-gapi/dist/angular-google-gapi.min.js",
      "app/bower_components/angular-local-storage/dist/angular-local-storage.js",
      "app/bower_components/ngprogress/build/ngprogress.min.js",
      "app/bower_components/ng-file-upload/ng-file-upload.js",
      "app/bower_components/oauth-js/dist/oauth.min.js",
      "app/bower_components/toastr/toastr.js",
      "app/bower_components/angular-bootstrap/ui-bootstrap-tpls.js",
      "app/bower_components/angular-mocks/angular-mocks.js",
      "app/bower_components/angular-payments/lib/angular-payments.js",
      "app/bower_components/angular-off-click/dist/angular-off-click.js",
      "app/bower_components/angular-input-masks/angular-input-masks-standalone.js",
      "bower_components/ng-intl-tel-input/dist/ng-intl-tel-input.js",
      "app/bower_components/international-phone-number/releases/international-phone-number.js",
      "app/bower_components/intl-tel-input/lib/libphonenumber/build/utils.js",
      "app/bower_components/intl-tel-input/build/js/intlTelInput.min.js",

      // endbower
      'app/scripts/app.js',
      'app/scripts/services/config.js',
      'app/scripts/services/services.js',
      'app/scripts/services/fileupload.js',
      'app/scripts/services/factory.js',
      'app/scripts/controllers/*.js',
      'test/spec/controllers/*.js'
    ],

    // list of files to exclude
    exclude: [
    ],


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    // reporters: ['progress', 'coverage'],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      'app/scripts/services/*.js': ['coverage'],
      'app/scripts/controllers/*.js': ['coverage']
    },

    // web server port
    port: 9092,

    // saves coverage reports
    coverageReporter: {
      // specify a common output directory
      dir: 'build/reports/coverage',
      reporters: [
        // reporters not supporting the `file` property
        { type: 'html', subdir: 'report-html' },
        { type: 'cobertura', subdir: 'cobertura' }
      ]
      //reporters : ['dots', 'junit', 'coverage']
    },

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress','coverage'],

    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,



    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
     // browsers: ['Chrome'],
       browsers: ['Chrome'],
    // you can define custom flags
    /*customLaunchers: {
      'PhantomJS_custom': {
        base: 'PhantomJS',
        options: {
          windowName: 'my-window',
          settings: {
            webSecurityEnabled: false
          },
        },
        flags: ['--load-images=true'],
        debug: true
      }
    },

    phantomjsLauncher: {
      // Have phantomjs exit if a ResourceError is encountered (useful if karma exits without killing phantom)
      exitOnResourceError: true
    },*/

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true
  });
};
